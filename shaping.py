#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lxml import etree
from pymongo import Connection
import MeCab
import re

def my_cut(t,p):
    x = p.search(t)

    if x:
        while 1:
            x = p.search(t)
            if x:
                t = t.replace(x.group(), '')
            else:
                break
    return t

def url_cut(t):
    p = re.compile(r"(?:https?|ftp):\/\/[A-Za-z0-9\/.-…]*")
    return my_cut(t,p)

def twitterid_cut(t):
    p = re.compile(r"(RT\ |)@[A-Za-z0-9_]*(:|)")

    return my_cut(t,p)

def client_filter(via):
    white = ['Twitter Web Client', 'Twitter for iPhone', 'Twitter for Android',]
    """
             'Janetter', 'twicca', 'TweetDeck', 'Keitai Web', u'Biyon≡(　ε:)', 'Twitter for iPad',
             u'Tweetbot for iΟS', 'jigtwi', u'Echofon', 'TheWorld for iOS', 'Tween', u'ついっぷる']
    """

    try:
        p = etree.fromstring(via)
        t = p .text
    except:
        return None

    if t in white:
        f = True
    else:
        f = False

    return f

def main():
    """
    shape the Twitter data of mondoDB to corpus for word2vec
    """

    # mongoDB
    connect = Connection('localhost', 27017)
    db = connect.tweets
    collect = db.tweets

    m = MeCab.Tagger ("-Ochasen")

    for data in collect.find():
        t = data[u'text']

        t = url_cut(t)
        t = twitterid_cut(t)

        f = False
        for i in data['entities'].values():
            if i:
                f = True

        if f:
            continue

        
        c = client_filter(data['source'])

        if not client_filter(data['source']):
            continue

        for w in m.parse(t.encode('utf-8')).split('\n'):
            if w == 'EOS':
                break

            w = w.split('\t')
            if w[1] == '#' or w[3].startswith('記号'):
                continue

            #if w[3].startswith(('名詞', '動詞', '形容詞')):
            print w[2],
        print
 

if __name__ == '__main__':
    main()
