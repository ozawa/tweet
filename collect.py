from pymongo import Connection
import twitter
from langdetect import detect

from datetime import datetime

def main():
    """
    collect tweets from public stream and store to mongoDB
    run every minitue with crontab
    """

    # mongoDB
    connect = Connection('localhost', 27017)
    db = connect.tweets
    collect = db.tweets

    # twitter
    api = twitter.Api(consumer_key='your_consumer_key',
                      consumer_secret='your_consumer_secret',
                      access_token_key='your_access_token_key',
                      access_token_secret='your_token_secret',
                      cache=None
                  )

    ts = api.GetStreamSample()    
    s = datetime.now()

    for t in ts:
        try:
            # language-decection
            # if detect(t[u'text']) == u'ja':
            if t[u'lang'] == u'ja':
                collect.save(t)
                
        except KeyError:
            pass
            
        
        if (datetime.now() -s).seconds >= 60:
            break
        
    ts.close()
    
if __name__ == '__main__':
    main()
